// https://adventofcode.com/2022/day/7

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let currentLocation = '/';
  const folders = [];
  rl.on('line', (input) => {
    // do stuf on each line

    // check if the line is a command
    if (input.slice(0, 1) === '$') {
      const cmd = input.slice(2, 4);
      if (cmd === 'cd') {
        const cdArg = input.slice(5);
        if (cdArg === '/') {
          currentLocation = '/';
        } else if (cdArg === '..') {
          let tmpArr = currentLocation.split('/');
          tmpArr = tmpArr.slice(0, tmpArr.length - 1);
          currentLocation = tmpArr.join('/');
          if (currentLocation === '') currentLocation = '/';
        } else {
          if (currentLocation[currentLocation.length - 1] === '/') currentLocation += `${cdArg}`;
          else currentLocation += `/${cdArg}`;
        }
      }

      if (cmd === 'ls') {
        const tmpArr = currentLocation.split('/');
        const folderName = tmpArr[tmpArr.length - 1];
        const folder = {
          path: currentLocation,
          name: folderName,
          size: 0
        }
        folders.push(folder);
      }
    } else {
      if (input.slice(0, 3) !== 'dir') {
        const splitedInput = input.split(' ');
        const fileSize = parseInt(splitedInput[0]);

        const currentFolders = folders.filter(elmt => currentLocation.includes(elmt.path));
        for (let i = 0; i < currentFolders.length; i++) {
          const currentFolder = currentFolders[i];
          currentFolder.size += fileSize;
        }
      }
    }
  })

  rl.on('close', () => {
    // final result 
    let result = 0;
    for (let i = 0; i < folders.length; i++) {
      const folder = folders[i];
      if (folder.size <= 100000) result += folder.size
    }
    console.log(result)
  })
}

await main();