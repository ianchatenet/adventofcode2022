// https://adventofcode.com/2022/day/3

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));
const JS_BASE_CHAR_CODE = 97 - 1; // JS charcode for "a" + 1 because we need 1 as first value not 0
const ALPHABET_LENGHT = 26;


async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let score = 0;

  rl.on('line', (input) => {
    const firstHalf = input.slice(0, input.length / 2);
    const secondHalf = input.slice(input.length / 2);

    for (let i = 0; i < firstHalf.length; i++) {
      const letter = firstHalf[i];
      const isInBoth = secondHalf.includes(letter);

      if (isInBoth) {

        let letterValue;
        if (letter === letter.toUpperCase()) {
          letterValue = letter.toLocaleLowerCase().charCodeAt(0) - JS_BASE_CHAR_CODE + ALPHABET_LENGHT;
        } else {
          letterValue = letter.charCodeAt(0) - JS_BASE_CHAR_CODE;
        }

        score += letterValue;
        return;
      }
    }
  })

  rl.on('close', () => {
    console.log(score);
  })
}

await main();