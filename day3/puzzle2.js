// https://adventofcode.com/2022/day/3

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));
const JS_BASE_CHAR_CODE = 97 - 1; // JS charcode for "a" + 1 because we need 1 as first value not 0
const ALPHABET_LENGHT = 26;


async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let score = 0;
  let savedLines = [];

  rl.on('line', (input) => {
    savedLines.push(input);
    if (savedLines.length === 3) {

      const firstLine = savedLines[0];
      const secondLine = savedLines[1];
      const thirdLine = savedLines[2];

      for (let i = 0; i < firstLine.length; i++) {
        const char = firstLine[i];

        const isInSecondLine = secondLine.includes(char);
        const isInThirdLine = thirdLine.includes(char);

        if (isInSecondLine === true && isInThirdLine === true) {
          let value;
          if (char === char.toUpperCase()) {
            value = char.toLocaleLowerCase().charCodeAt(0) - JS_BASE_CHAR_CODE + ALPHABET_LENGHT;
          } else {
            value = char.charCodeAt(0) - JS_BASE_CHAR_CODE;
          }

          score += value;
          break;
        }
      }
      savedLines = [];
    }
  })

  rl.on('close', () => {
    console.log(score);
  })
}

await main();