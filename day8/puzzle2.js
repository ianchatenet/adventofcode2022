// https://adventofcode.com/2022/day/8

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  const forest = [];
  rl.on('line', (input) => {
    const treeLine = [];
    for (let i = 0; i < input.length; i++) {
      const char = input[i];
      treeLine.push(parseInt(char));
    }
    forest.push(treeLine)
  })

  rl.on('close', () => {
    // final result 
    let result = 0;

    for (let i = 0; i < forest.length; i++) {
      const treeLine = forest[i];
      for (let j = 0; j < treeLine.length; j++) {
        const tree = treeLine[j];

        let leftScenicScore = 0;
        for (let k = j - 1; k >= 0; k--) {
          const leftTree = treeLine[k];
          leftScenicScore++;
          if (tree <= leftTree) break;
        }

        let rightScenicScore = 0;
        for (let k = j + 1; k < treeLine.length; k++) {
          const rightTree = treeLine[k];
          rightScenicScore++;
          if (tree <= rightTree) break;
        }

        let upScenicScore = 0;
        for (let k = i - 1; k >= 0; k--) {
          const upTree = forest[k][j];
          upScenicScore++;
          if (tree <= upTree) break;
        }

        let bottomScenicScore = 0;
        for (let k = i + 1; k < forest.length; k++) {
          const bottomTree = forest[k][j];
          bottomScenicScore++;
          if (tree <= bottomTree) break;
        }

        const treeScenicScore = leftScenicScore * rightScenicScore * upScenicScore * bottomScenicScore;
        if (treeScenicScore > result) result = treeScenicScore;
      }
    }
    console.log(result)
  })
}

await main();