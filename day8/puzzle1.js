// https://adventofcode.com/2022/day/8

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  const forest = [];
  rl.on('line', (input) => {
    const treeLine = [];
    for (let i = 0; i < input.length; i++) {
      const char = input[i];
      treeLine.push(parseInt(char));
    }
    forest.push(treeLine)
  })

  rl.on('close', () => {
    // final result 
    let visibleTrees = 0;
    for (let i = 0; i < forest.length; i++) {
      const treeLine = forest[i];
      if (i === 0 || i === forest.length - 1) visibleTrees += treeLine.length;
      else {
        for (let j = 0; j < treeLine.length; j++) {
          const tree = treeLine[j];
          if (j === 0 || j === forest.length - 1) visibleTrees++;
          else {
            let isVisibleLeft = true;
            for (let k = j - 1; k >= 0; k--) {
              const leftTree = treeLine[k];
              if (leftTree >= tree) isVisibleLeft = false;
            }

            let isVisibleRight = true;
            for (let k = j + 1; k < treeLine.length; k++) {
              const rightTree = treeLine[k];
              if (rightTree >= tree) isVisibleRight = false;
            }

            let isVisibleTop = true;
            for (let k = i - 1; k >= 0; k--) {
              const upTree = forest[k][j];
              if (upTree >= tree) isVisibleTop = false;
            }

            let isVisibleBottom = true;
            for (let k = i + 1; k < forest.length; k++) {
              const bottomTree = forest[k][j];
              if (bottomTree >= tree) isVisibleBottom = false;
            }
            const isVisible = isVisibleLeft || isVisibleRight || isVisibleTop || isVisibleBottom;
            if (isVisible === true) visibleTrees++;
          }
        }
      }
    }
    console.log(visibleTrees)
  })
}

await main();