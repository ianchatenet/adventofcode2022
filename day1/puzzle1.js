// https://adventofcode.com/2022/day/1

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let elfNumber = 0;
  let elfCalories = [];

  rl.on('line', (input) => {
    if (input === '') {
      elfNumber++;
    }

    if (!elfCalories[elfNumber]) elfCalories[elfNumber] = 0;
    elfCalories[elfNumber] = elfCalories[elfNumber] + Number(input);
  })

  rl.on('close', () => {
    const totalCal = Math.max.apply(null, elfCalories);
    const elfNbr = elfCalories.findIndex(elmt => elmt === totalCal);
    console.log(`Elf carrying the most calories is the number ${elfNbr + 1}, with a total of ${totalCal}`);
  })
}

await main();