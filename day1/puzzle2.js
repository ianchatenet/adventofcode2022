// https://adventofcode.com/2022/day/1#part2

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let elfNumber = 0;
  let elfCalories = [];

  rl.on('line', (input) => {
    if (input === '') {
      elfNumber++;
    }

    if (!elfCalories[elfNumber]) elfCalories[elfNumber] = 0;
    elfCalories[elfNumber] = elfCalories[elfNumber] + Number(input);
  })

  rl.on('close', () => {
    elfCalories.sort((a, b) => a - b);
    const thirdHigherElf = elfCalories.slice(elfCalories.length - 3);
    const result = thirdHigherElf.reduce((acc, value) => acc + value, 0);
    console.log(result)
  })
}

main();