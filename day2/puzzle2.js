// https://adventofcode.com/2022/day/2

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';
const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

const rock = {
  value: 1,
  identifiers: ['A', 'X'],
  getFoeId: (foeResult) => {
    if (foeResult === 'X') return scissors.identifiers[0];
    if (foeResult === 'Y') return rock.identifiers[0];
    if (foeResult === 'Z') return paper.identifiers[0];
  }
};

const paper = {
  value: 2,
  identifiers: ['B', 'Y'],
  getFoeId: (foeResult) => {
    if (foeResult === 'X') return rock.identifiers[0];
    if (foeResult === 'Y') return paper.identifiers[0];
    if (foeResult === 'Z') return scissors.identifiers[0];
  }
};

const scissors = {
  value: 3,
  identifiers: ['C', 'Z'],
  getFoeId: (foeResult) => {
    if (foeResult === 'X') return paper.identifiers[0];
    if (foeResult === 'Y') return scissors.identifiers[0];
    if (foeResult === 'Z') return rock.identifiers[0];
  }
};

function factory(identifiers) {
  if (paper.identifiers.includes(identifiers)) return paper;
  if (scissors.identifiers.includes(identifiers)) return scissors;
  if (rock.identifiers.includes(identifiers)) return rock;
}

async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let myScore = 0;

  rl.on('line', (input) => {
    const splitedInput = input.split(' ');
    const foeId = splitedInput[0];
    const expectedResult = splitedInput[1];

    const foeShape = factory(foeId);

    const myId = foeShape.getFoeId(expectedResult);
    const myShape = factory(myId);

    if (expectedResult === 'Z') myScore = myScore + myShape.value + 6;
    if (expectedResult === 'X') myScore = myScore + myShape.value;
    if (expectedResult === 'Y') myScore = myScore + myShape.value + 3;
  })

  rl.on('close', () => {
    console.log(`my score: ${myScore}`);
  })
}

main();