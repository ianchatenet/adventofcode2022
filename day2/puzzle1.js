// https://adventofcode.com/2022/day/2

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';
const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

const rock = {
  value: 1,
  identifiers: ['A', 'X'],
  compare: (foeId) => {
    if (scissors.identifiers.includes(foeId)) return 1;
    if (paper.identifiers.includes(foeId)) return -1;
    if (rock.identifiers.includes(foeId)) return 0;
  }
};

const paper = {
  value: 2,
  identifiers: ['B', 'Y'],
  compare: (foeId) => {
    if (rock.identifiers.includes(foeId)) return 1;
    if (scissors.identifiers.includes(foeId)) return -1;
    if (paper.identifiers.includes(foeId)) return 0;
  }
};

const scissors = {
  value: 3,
  identifiers: ['C', 'Z'],
  compare: (foeId) => {
    if (paper.identifiers.includes(foeId)) return 1;
    if (rock.identifiers.includes(foeId)) return -1;
    if (scissors.identifiers.includes(foeId)) return 0;
  }
};

function factory(identifiers) {
  if (paper.identifiers.includes(identifiers)) return paper;
  if (scissors.identifiers.includes(identifiers)) return scissors;
  if (rock.identifiers.includes(identifiers)) return rock;
}


async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let myScore = 0;

  rl.on('line', (input) => {
    const splitedInput = input.split(' ');
    const foeId = splitedInput[0];
    const myId = splitedInput[1];

    const myShape = factory(myId);
    const roundResult = myShape.compare(foeId);

    if (roundResult === 1) myScore = myScore + myShape.value + 6;
    if (roundResult === -1) myScore = myScore + myShape.value;
    if (roundResult === 0) myScore = myScore + myShape.value + 3;
  })

  rl.on('close', () => {
    console.log(`my score: ${myScore}`);
  })
}

main();