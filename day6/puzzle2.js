// https://adventofcode.com/2022/day/6

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  rl.on('line', (input) => {
    // do stuf on each line
    let savedChar = [];

    for (let i = 0; i < input.length; i++) {
      const char = input[i];
      savedChar.push(char);
      if (savedChar.length === 15) {
        savedChar.shift();
        const savedCharUniq = new Set([...savedChar]);

        if (savedCharUniq.size === 14) {
          console.log(`result is: ${i + 1}`)
          return;
        };
      }
    }

  })

  rl.on('close', () => {
    // final result 
  })
}

await main();