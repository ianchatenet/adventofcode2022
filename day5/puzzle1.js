// https://adventofcode.com/2022/day/5

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let allStacks = [];
  let isInBaseStack = true;

  const indexStackOne = 2;

  rl.on('line', (input) => {
    if (input === '') {
      isInBaseStack = false;
      return;
    }

    if (isInBaseStack === true) {
      // code to get the base stack
      for (let i = 0; i < input.length; i++) {
        const char = input[i];

        if (/^[a-z]+$/i.test(char)) {
          const stackIndex = Math.floor((i - indexStackOne) / 4) + 1;

          if (allStacks[stackIndex] === undefined) allStacks[stackIndex] = [];
          allStacks[stackIndex].unshift(char);
        }
      }
    } else {
      // code to read instructions
      const match = input.match(/move (?<moving>.*) from (?<from>.*) to (?<to>.*)/);
      const movingQuantity = match.groups.moving;
      const fromStack = match.groups.from - 1;
      const toStack = match.groups.to - 1;

      for (let i = 0; i < movingQuantity; i++) {
        const movingElement = allStacks[fromStack].pop();
        allStacks[toStack].push(movingElement);
      }
    }
  })

  rl.on('close', () => {
    // final result 
    let result = '';
    for (let i = 0; i < allStacks.length; i++) {
      const stack = allStacks[i];
      result += stack[stack.length - 1];
    }
    console.log(result)
  })
}

await main();