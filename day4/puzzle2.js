// https://adventofcode.com/2022/day/4

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));


function isOverlaping(firstRange, secondRange) {
  const startIsLower = firstRange.start <= secondRange.start && secondRange.start <= firstRange.end;
  const endIsHigher = firstRange.end >= secondRange.end && secondRange.end >= firstRange.start;
  return startIsLower || endIsHigher;
}


async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let counter = 0;

  rl.on('line', (input) => {
    // do stuf on each line
    const splited = input.split(',');
    const firstElf = splited[0];
    const secondElf = splited[1];

    const splitedFirst = firstElf.split('-');
    const splitedSecond = secondElf.split('-');

    const firstElfRange = {
      start: Number(splitedFirst[0]),
      end: Number(splitedFirst[1]),
    };

    const secondElfRange = {
      start: Number(splitedSecond[0]),
      end: Number(splitedSecond[1]),
    };

    const isSecondOverlaping = isOverlaping(firstElfRange, secondElfRange);
    const isFIrstOverlaping = isOverlaping(secondElfRange, firstElfRange);

    if (isSecondOverlaping || isFIrstOverlaping) counter++;
  })

  rl.on('close', () => {
    console.log(`counter ${counter}`);
  })
}

await main();