// https://adventofcode.com/2022/day/4

import * as url from 'url';
import { join } from 'path';
import { createReadStream } from 'fs';
import { createInterface } from 'readline';

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));


function isInside(firstRange, secondRange) {
  const startIsLower = firstRange.start <= secondRange.start;
  const endIsHigher = firstRange.end >= secondRange.end;
  return startIsLower && endIsHigher;
}


async function main() {
  const filePath = join(__dirname, 'data');
  const readStream = createReadStream(filePath);
  const rl = createInterface({ input: readStream });

  let counter = 0;

  rl.on('line', (input) => {
    // do stuf on each line
    const splited = input.split(',');
    const firstElf = splited[0];
    const secondElf = splited[1];

    const splitedFirst = firstElf.split('-');
    const splitedSecond = secondElf.split('-');

    const firstElfRange = {
      start: Number(splitedFirst[0]),
      end: Number(splitedFirst[1]),
    };

    const secondElfRange = {
      start: Number(splitedSecond[0]),
      end: Number(splitedSecond[1]),
    };

    const isSecondInFirst = isInside(firstElfRange, secondElfRange);
    const isFirstInSecond = isInside(secondElfRange, firstElfRange);
    if (isSecondInFirst || isFirstInSecond) counter++;
  })

  rl.on('close', () => {
    console.log(`counter ${counter}`);
  })
}

await main();